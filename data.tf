data "google_compute_image" "centos" {
  family  = "centos-8"
  project = "centos-cloud"
}

data "google_compute_network" "workshop" {
  name = "workshop"
}

data "google_service_account" "compute" {
  account_id = "compute-instance"
}

output "image" {
  value = data.google_compute_image.centos.name
}

output "network" {
  value = data.google_compute_network.workshop.self_link
}

output "service_account" {
  value = data.google_service_account.compute.email
}
